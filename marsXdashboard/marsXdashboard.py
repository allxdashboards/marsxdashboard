import streamlit as st
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from datetime import datetime
from streamlit_extras.add_vertical_space import add_vertical_space

st.set_page_config(layout="wide")




def main():
    col00, col001 = st.columns(2)
    with col00:
        st.title(' Mars X-Dashboard')
        st.write('This Website brings various information on Mars missions together, and with the help of graphs and graphics visualizes it for easy understanding. . ')
    with col001:
        st.subheader('Next mars mission:')
        st.write('The Next mars mission is NASAs EscaPade, consisting of twin spacecrafts called Blue and Gold, which will invistigate the solar storm and its prossible contribution to the loss of the marsian atmosphere. This mission is to be launched in August 2024. ')
        

    
    col01, col02,  = st.columns(2)

    with col01: 
         st.image('https://images.twinkl.co.uk/tw1n/image/private/t_630_eco/image_repo/62/d6/mars_ver_3.jpg')

    with col02: 
       st.image('https://escapade.ssl.berkeley.edu/wp-content/uploads/2021/12/ESCAPADE-Turntable-Loop_1920px_1-1-1.gif')
    
    col9, col99 = st.columns(2)
    with col99:
       st.link_button ('Want to know more about EscaPade?', 'https://nssdc.gsfc.nasa.gov/nmc/spacecraft/display.action?id=ESCAPADE' )

     
        
         
    col11, col12, = st.columns(2)
   
    with col11:
        # add_n_lines =st.slider
        # add_vertical_space(3)
        # Data
        success_rate = 52.24  # Example success rate
        failure_rate = 47.76

    # Example failure rate
        rates = [success_rate, failure_rate]
        labels = ['Success', 'Failure',]
        colors = ['#66b3ff', '#ff9999',]



        
        
   
        st.subheader('Mars mission Success Rate')
        st.write('This is an overview of the success and failure rates of Mars missions. This shows the overall outcome of all Mars missions ever launched.')

        fig_failure, ax = plt.subplots()
        ax.pie(rates, labels=labels, colors=colors, autopct='%1.1f%%', startangle=90)
        ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

        st.pyplot(fig_failure)

        col7,col8 = st.columns(2)
        with col7:
            st.link_button("Furhter information on all Mars missions?", "https://en.wikipedia.org/wiki/List_of_missions_to_Mars")

    


    with col12: 
        
        # add_n_lines =st.slider
       #  add_vertical_space(8)
        
        st.subheader('Types of missions')
        st.write('This displays the distribution of Mars missions by type, giving insight into the diversity of mars exploration approaches. ')

        rover_rate  = 9.33  # Example success rate
        flyby_rate = 22.67
        lander_rate = 22.67
        helicopter_rate = 1.33
        orbiter_rate = 37.33
        penetrator_rate = 6.67

    # Example failure rate
        rates = [rover_rate, flyby_rate, lander_rate, helicopter_rate, orbiter_rate, penetrator_rate]
        labels = ['rover', 'flyby', 'lander', 'helicopter', 'orbiter', 'penetrator']
        colors = ['#66b3ff', '#ff9999','#ff8654', '#ff3668', '#ff5325', '#ff1111']

        fig_types, ax = plt.subplots()
        ax.pie(rates, labels=labels, colors=colors, autopct='%1.1f%%', startangle=90)
        ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    
        plt.show()
        st.pyplot(fig_types)


    col41, col42= st.columns(2)
    with col41: 
         st.subheader('Signal time')
         st.write('This graph visualizes the singal time from earth to mars,depending on the month. These tracks aid mission planning and data transmission scheduling. ')
    with col42: 
         st.subheader('Mars missions by agency')
         st.write('Shows Mars missions of each  top leading space agency and their successes, which gives insight into agency performance and mars exploration challenges.')
            
    col21, col22 = st.columns(2)
    


    with col21:
         
        
        data= pd.DataFrame({
        'month':(datetime(2024, 1, 1),  datetime(2024, 2, 1), datetime(2024, 3, 1), datetime(2024, 4, 1), datetime(2024, 5, 1), datetime(2024, 6, 1), datetime(2024, 7, 1), datetime(2024, 8, 1), datetime(2024, 9, 1), datetime(2024, 10, 1), datetime(2024, 11, 1), datetime(2024, 12, 1), datetime(2025, 1, 1), datetime(2025, 2, 1), datetime(2025, 3, 1), datetime(2025, 4, 1), datetime(2025, 5, 1), datetime(2025, 6, 1), datetime(2025, 7, 1), datetime(2025, 8, 1), datetime(2025, 9, 1), datetime(2025, 10, 1), datetime(2025, 11, 1)),
        'x':(355  ,337 , 320 , 302, 288, 269, 250, 227, 200, 168, 135, 110, 99, 115, 150, 195, 230, 267, 302, 330, 345, 355, 361 ),
        'y': (19 , 18 , 17 , 16 , 16,  14 , 13 , 12 , 11 , 9 , 7 , 6 , 5 , 6 , 8 , 10 , 12 , 14 , 16 , 18 , 19 , 19 , 20  )
            })

    

        fig_signal, ax= plt.subplots()
        ax.plot(data['month'],data['y'])
        ax.set_xlabel('month')
        ax.set_ylabel('minutes')
        
    


        st.pyplot(fig_signal)

    with col22: 
        

        agencies = ("ESA", "NASA", "Roscosmos", 'OKB-1', 'ISRO', 'ISAS', 'MBRSC', 'CNSA')
        missions = {
        'all mars missions': (4, 24, 3, 14, 1, 1, 1, 1),
        'succesful': (1, 14, 0, 3, 0, 1, 0, 0),
        'unknown': (1, 6, 1, 0, 0, 0, 0, 1),
     }
    

        x = np.arange(len(agencies))  # the label locations
        width = 0.25  # the width of the bars
        multiplier = 0

        fig_agencies, ax = plt.subplots(layout='constrained')

        for attribute, measurement in missions.items():
            offset = width * multiplier
            rects = ax.bar(x + offset, measurement, width, label=attribute)
            ax.bar_label(rects, padding=3)
            multiplier += 1

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Number of missions')
        ax.set_xticks(x + width, agencies)
        ax.legend(loc='upper left', ncols=3)
        ax.set_ylim(0, 35)

        st.pyplot(fig_agencies)
        
        


    col31, col32 = st.columns(2)
    with col31:
            st.subheader('Typical trajectory of satelite around sun')
            st.image('https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Animation_of_2001_Mars_Odyssey_trajectory_around_Sun.gif/500px-Animation_of_2001_Mars_Odyssey_trajectory_around_Sun.gif')
            st.link_button('What is a satelite trajectory?', 'https://www.isro.gov.in/mission_mars_orbiter_mission_profile.html#:~:text=The%20flight%20path%20is%20roughly,an%20angle%20of%20approximately%2044o.')
    with col32:
         st.subheader('The Orbits of current mars missions')
         st.image('https://mars.nasa.gov/files/home/MarsNow-home2.jpg')

    # data=pd.DataFrame({
    #     'Mission' : (Perserverance, Tianwen-1 & Zhuronng, Hope, ExoMars Trace, Maven, Curiosity, Renessaince, Mars Express, Odyssey) ,
    #     'Agency'  :( NASA, CNSA, UAE, ESA & Roscosmos, NASA, NASA, NASA, ESA, NASA) ,
    #     'Active since:': (2020, 2020, 2020, 2016, 2013, 2012, 2006, 2003, 2001), 
    # })
    col50, col51 = st.columns(2)
    with col50:
        
        st.subheader('Current active missions')
        # Sample data
        data = {
            "Mission": ["Peserverance", "Tianwen-1 & Zhurong", "Hope", "ExoMars"],
        "Agency": ["NASA", "CNSA", "UAE", "ESA"],
        "Type": ["Rover", "Orbiter & Rover", "Orbiter", "Orbiter"],
        "Active since:": [2020, 2020, 2020, 2016],
        "Image": [
        '<img src="https://mars.nasa.gov/layout/mars2020/images/PIA23764-RoverNamePlateonMars-web.jpg" width="160">',
        '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Zhurong-with-lander-selfie.png/1200px-Zhurong-with-lander-selfie.png" width="160">',
        '<img src="https://cdn.mos.cms.futurecdn.net/FRqpFcnobky4VWCH3VquG.jpg" width="160">',
        '<img src="https://www.ralspace.stfc.ac.uk/Gallery/marsexpresss.jpg" width="160">',
       ]
     }
        # Create DataFrame
        df = pd.DataFrame(data)

        # Convert DataFrame to HTML
        html_table = df.to_html(escape=False, index=False)

        # Display HTML table
        st.markdown(html_table, unsafe_allow_html=True)
       

   

    with col51:
        add_n_lines = st.slider
        add_vertical_space(3)
        data= {
        "Mission": [ "Maven", "Curiosity", "Renessaince", "Mars Express", "Odyssey"],
        "Agency": [ "NASA", "NASA", "NASA", "ESA", "NASA"],
        "Type": ["Orbiter", "Rover", "Orbiter", "Orbiter", "Orbiter"],
        "Active since:": [ 2013, 2012, 2006, 2003, 2001],
        "Image": [
        '<img src="https://www.nasa.gov/wp-content/uploads/2017/04/maven_mars_limb-horizontal.png?w=2000" width="160">',
        '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/NASA_Mars_Rover.jpg/1200px-NASA_Mars_Rover.jpg" width="160">',
        '<img src="https://upload.wikimedia.org/wikipedia/commons/5/58/Mars_Reconnaissance_Orbiter%2C_front_view%2C_artist%27s_concept_%28PIA07245%29.jpg" width="160">',
        '<img src="https://planetary.s3.amazonaws.com/web/assets/pictures/_1200x630_crop_center-center_82_none/mars-express_art.jpg?mtime=1585760127" width="160">',
        '<img src="https://www.solarstorms.org/wp-content/uploads/2017/04/mars-odyssey.jpg" width="160">']

        }

        

     # Create DataFrame
        df = pd.DataFrame(data)

        # Convert DataFrame to HTML
        html_table = df.to_html(escape=False, index=False)

        # Display HTML table
        st.markdown(html_table, unsafe_allow_html=True)

    col888,col88 = st.columns(2)

    with col888:

        st.subheader('Travel Information of Rovers')
        # Sample data (replace with actual rover data)
        rover_data = {
        'Rover': ['Curiosity', 'Opportunity', 'Perseverance', 'Spirit'],
        'Distance driven (km)': [28.4, 28., 24.9, 7.73],
        'Active Duration (weeks)': [573, 730, 156, 312],
        'Speed (m/h)': [139.999, 179.999 , 119.999, 179.999 ]
        }

     # Create a DataFrame
        df = pd.DataFrame(rover_data)

        # Set up the figure and axis
        fig, ax = plt.subplots()

        # Define bar width and positions
        bar_width = 0.2
        bar_positions = range(len(df))

        # Plot bars for each metric
        for i, col in enumerate(['Distance driven (km)', 'Active Duration (weeks)', 'Speed (m/h)']):
         ax.bar([p + i * bar_width for p in bar_positions], df[col], bar_width, label=col)

            # Set x-axis labels and tick positions
         ax.set_xticks([p + bar_width for p in bar_positions])
         ax.set_xticklabels(df['Rover'])
         ax.set_xlabel('Rover')

         # Add legend and labels
         ax.legend()
         ax.set_ylabel('Value')
         ax.set_title('Mars Rover Comparison')

        # Display the chart in Streamlit
        st.pyplot(fig)

    with col88:

        st.subheader('Map of Mars Missions') 
        st.image('https://www.nsta.org/sites/default/files/2020-08/SS_68_Figure2.jpg')
      


            

   
        
        
if __name__ == '__main__':
        main()